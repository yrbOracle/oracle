# 软件工程四班

# 202010414423

#  杨润斌

# 实验1：SQL语句的执行计划分析与优化

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。


## 查询：查询两个国家（美国和英国）部门总数，部门人数和平均工资

### 查询1：

set autotrace on
SELECT 
    c.country_name,
    COUNT(DISTINCT d.department_id) AS "部门总数",
    COUNT(e.employee_id) AS "部门总人数",
    AVG(e.salary) AS "平均工资"
FROM 
    hr.locations l
    JOIN hr.departments d ON l.location_id = d.location_id
    JOIN hr.employees e ON d.department_id = e.department_id
    JOIN hr.countries c ON l.country_id = c.country_id
WHERE 
    c.country_name IN ('United States of America', 'United Kingdom')
GROUP BY 
 14      c.country_name;

COUNTRY_NAME                               部门总数 部门总人数   平均工资
---------------------------------------- ---------- ---------- ----------
United Kingdom					  2	    35 8885.71429
United States of America			  7	    68 5064.94118


执行计划
----------------------------------------------------------
Plan hash value: 1847178000

--------------------------------------------------------------------------------
-------------------------

| Id  | Operation			    | Name		| Rows	| Bytes
| Cost (%CPU)| Time	|

--------------------------------------------------------------------------------
-------------------------

|   0 | SELECT STATEMENT		    |			|     2 |   122
|     6  (17)| 00:00:01 |

|   1 |  HASH GROUP BY			    |			|     2 |   122
|     6  (17)| 00:00:01 |

|   2 |   VIEW				    | VW_DAG_0		|     6 |   366
|     6  (17)| 00:00:01 |

|   3 |    HASH GROUP BY		    |			|     6 |   354
|     6  (17)| 00:00:01 |

|   4 |     NESTED LOOPS		    |			|    39 |  2301
|     6  (17)| 00:00:01 |

|   5 |      NESTED LOOPS		    |			|    40 |  2301
|     6  (17)| 00:00:01 |

|   6 |       VIEW			    | VW_GBF_17 	|     4 |   208
|     4  (25)| 00:00:01 |

|   7 |        HASH GROUP BY		    |			|     4 |   100
|     4  (25)| 00:00:01 |

|   8 | 	NESTED LOOPS		    |			|     4 |   100
|     3   (0)| 00:00:01 |

|   9 | 	 NESTED LOOPS		    |			|    12 |   100
|     3   (0)| 00:00:01 |

|  10 | 	  NESTED LOOPS		    |			|     3 |    54
|     2   (0)| 00:00:01 |

|  11 | 	   VIEW 		    | index$_join$_001	|    23 |   138
|     2   (0)| 00:00:01 |

|* 12 | 	    HASH JOIN		    |			|	|
|	     |		|

|  13 | 	     INDEX FAST FULL SCAN   | LOC_COUNTRY_IX	|    23 |   138
|     1   (0)| 00:00:01 |

|  14 | 	     INDEX FAST FULL SCAN   | LOC_ID_PK 	|    23 |   138
|     1   (0)| 00:00:01 |

|* 15 | 	   INDEX UNIQUE SCAN	    | COUNTRY_C_ID_PK	|     1 |    12
|     0   (0)| 00:00:01 |

|* 16 | 	  INDEX RANGE SCAN	    | DEPT_LOCATION_IX	|     4 |
|     0   (0)| 00:00:01 |

|  17 | 	 TABLE ACCESS BY INDEX ROWID| DEPARTMENTS	|     1 |     7
|     1   (0)| 00:00:01 |

|* 18 |       INDEX RANGE SCAN		    | EMP_DEPARTMENT_IX |    10 |
|     0   (0)| 00:00:01 |

|  19 |      TABLE ACCESS BY INDEX ROWID    | EMPLOYEES 	|    10 |    70
|     1   (0)| 00:00:01 |

--------------------------------------------------------------------------------
-------------------------


Predicate Information (identified by operation id):
---------------------------------------------------

  12 - access(ROWID=ROWID)
  15 - access("L"."COUNTRY_ID"="C"."COUNTRY_ID")
       filter("C"."COUNTRY_NAME"='United Kingdom' OR "C"."COUNTRY_NAME"='United
States of

	      America')
  16 - access("L"."LOCATION_ID"="D"."LOCATION_ID")
  18 - access("ITEM_1"="E"."DEPARTMENT_ID")


统计信息
----------------------------------------------------------
	 83  recursive calls
	  0  db block gets
	 57  consistent gets
	  0  physical reads
	  0  redo size
	941  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

这个查询它使用了 JOIN 子句来连接 locations、departments、employees 和 countries 表，并使用了 WHERE 子句来过滤出美国和英国的部门。然后使用 GROUP BY 子句按照国家名称分组，并使用聚合函数 COUNT 和 AVG 来计算部门总数、部门人数和平均工资。
COUNTRY_NAME              部门总数 部门总人数   平均工资
----------------------------- ---------- ---------- ----------
United Kingdom					  2	      35      8885.71429
United States of America		  7	      68      5064.94118

## 优化指导结果

 为此语句找到了性能更好的执行计划。

  Recommendation (estimated benefit<=10%)
  ---------------------------------------
  - 考虑接受推荐的 SQL 概要文件。
    execute dbms_sqltune.accept_sql_profile(task_name => 'staName5791',
            task_owner => 'HR', replace => TRUE);


```python
为此语句找到了性能更好的执行计划。

  Recommendation (estimated benefit: 32.44%)
  ------------------------------------------
  - 考虑接受推荐的 SQL 概要文件。
    execute dbms_sqltune.accept_sql_profile(task_name => 'staName9457',
            task_owner => 'HR', replace => TRUE);
```


      Cell In[2], line 1
        为此语句找到了性能更好的执行计划。
                        ^
    SyntaxError: invalid character '。' (U+3002)
    


### 查询2
set autotrace on
SELECT 
    country_name, 
    SUM(department_count) AS "部门总数",
    SUM(employee_count) AS "部门总人数",
    AVG(salary) AS "平均工资"
FROM (
    SELECT 
        c.country_name,
        d.department_id,
        COUNT(DISTINCT d.department_id) OVER (PARTITION BY c.country_name) AS department_count,
        COUNT(e.employee_id) OVER (PARTITION BY c.country_name, d.department_id) AS employee_count,
        e.salary
    FROM 
        hr.locations l
        JOIN hr.departments d ON l.location_id = d.location_id
        JOIN hr.employees e ON d.department_id = e.department_id
        JOIN hr.countries c ON l.country_id = c.country_id
    WHERE 
        c.country_name IN ('United States of America', 'United Kingdom')
)
GROUP BY 
 22      country_name;

COUNTRY_NAME                               部门总数 部门总人数   平均工资
---------------------------------------- ---------- ---------- ----------
United Kingdom					 70	  1157 8885.71429
United States of America			476	  2136 5064.94118


执行计划
----------------------------------------------------------
Plan hash value: 64554509

--------------------------------------------------------------------------------
-----------------------

| Id  | Operation			  | Name	      | Rows  | Bytes |
Cost (%CPU)| Time     |

--------------------------------------------------------------------------------
-----------------------

|   0 | SELECT STATEMENT		  |		      |     2 |    96 |
    6  (17)| 00:00:01 |

|   1 |  SORT GROUP BY NOSORT		  |		      |     2 |    96 |
    6  (17)| 00:00:01 |

|   2 |   VIEW				  |		      |    15 |   720 |
    6  (17)| 00:00:01 |

|   3 |    WINDOW SORT			  |		      |    15 |   540 |
    6  (17)| 00:00:01 |

|   4 |     NESTED LOOPS		  |		      |    15 |   540 |
    5	(0)| 00:00:01 |

|   5 |      NESTED LOOPS		  |		      |    40 |   540 |
    5	(0)| 00:00:01 |

|   6 |       NESTED LOOPS		  |		      |     4 |   100 |
    3	(0)| 00:00:01 |

|   7 |        NESTED LOOPS		  |		      |     3 |    54 |
    2	(0)| 00:00:01 |

|   8 | 	VIEW			  | index$_join$_002  |    23 |   138 |
    2	(0)| 00:00:01 |

|*  9 | 	 HASH JOIN		  |		      |       |       |
	   |	      |

|  10 | 	  INDEX FAST FULL SCAN	  | LOC_COUNTRY_IX    |    23 |   138 |
    1	(0)| 00:00:01 |

|  11 | 	  INDEX FAST FULL SCAN	  | LOC_ID_PK	      |    23 |   138 |
    1	(0)| 00:00:01 |

|* 12 | 	INDEX UNIQUE SCAN	  | COUNTRY_C_ID_PK   |     1 |    12 |
    0	(0)| 00:00:01 |

|  13 |        TABLE ACCESS BY INDEX ROWID| DEPARTMENTS       |     1 |     7 |
    1	(0)| 00:00:01 |

|* 14 | 	INDEX RANGE SCAN	  | DEPT_LOCATION_IX  |     4 |       |
    0	(0)| 00:00:01 |

|* 15 |       INDEX RANGE SCAN		  | EMP_DEPARTMENT_IX |    10 |       |
    0	(0)| 00:00:01 |

|  16 |      TABLE ACCESS BY INDEX ROWID  | EMPLOYEES	      |     4 |    44 |
    1	(0)| 00:00:01 |

--------------------------------------------------------------------------------
-----------------------


Predicate Information (identified by operation id):
---------------------------------------------------

   9 - access(ROWID=ROWID)
  12 - access("L"."COUNTRY_ID"="C"."COUNTRY_ID")
       filter("C"."COUNTRY_NAME"='United Kingdom' OR "C"."COUNTRY_NAME"='United
States of

	      America')
  14 - access("L"."LOCATION_ID"="D"."LOCATION_ID")
  15 - access("D"."DEPARTMENT_ID"="E"."DEPARTMENT_ID")

Note
-----
   - this is an adaptive plan


统计信息
----------------------------------------------------------
	428  recursive calls
	  0  db block gets
	580  consistent gets
	  0  physical reads
	  0  redo size
	944  bytes sent via SQL*Net to client
	608  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	 38  sorts (memory)
	  0  sorts (disk)
	  2  rows processed

这个SQL查询语句首先从hr.locations表开始，使用JOIN连接hr.departments、hr.employees和hr.countries表，以获取每个国家、每个部门和每个员工的相关信息。然后，它过滤出符合条件的国家（即美国和英国），并计算每个国家每个部门的总人数、总部门数和平均工资。最后，通过对每个国家进行分组，使用SUM和AVG聚合函数进行汇总，得出最终结果。

COUNTRY_NAME              部门总数 部门总人数   平均工资
----------------------------- ---------- ---------- ----------
United Kingdom					  2	      35      8885.71429
United States of America		  7	      68      5064.94118


## 分析结果

这个查询语句和前面的查询语句相比，是比较优化的。它使用了一个子查询来生成部门总数、部门总人数和平均工资，并且使用了窗口函数来计算每个部门的员工数量。

该查询语句首先从hr.locations表开始，然后连接hr.departments、hr.employees和hr.countries表。使用WHERE子句过滤出美国和英国两个国家的数据。接着，它使用子查询计算每个部门的总人数、总部门数和平均工资，然后对这些数据进行汇总并计算总体的部门总数、总人数和平均工资。

与前面一个查询语句相比，这个查询语句避免了使用了昂贵的DISTINCT关键字，以及使用了更少的表连接。它还利用了窗口函数来避免了重复的聚合操作，从而提高了查询性能。

总的来说，这个查询语句是一个更好的选择，因为它更加优化和高效。
