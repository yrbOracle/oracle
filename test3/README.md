# 实验3：创建分区表

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验过程

```sql
CREATE TABLE orders 
(
 order_id NUMBER(9, 0) NOT NULL
 , customer_name VARCHAR2(40 BYTE) NOT NULL 
 , customer_tel VARCHAR2(40 BYTE) NOT NULL 
 , order_date DATE NOT NULL 
 , employee_id NUMBER(6, 0) NOT NULL 
 , discount NUMBER(8, 2) DEFAULT 0 
 , trade_receivable NUMBER(8, 2) DEFAULT 0 
 , CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE (   BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL 

PARTITION BY RANGE (order_date) 
(
 PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE USERS
 PCTFREE 10 
 INITRANS 1 
 STORAGE 
( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT 
) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE USERS
, PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE USERS
);
--以后再逐年增加新年份的分区
ALTER TABLE orders ADD PARTITION partition_before_2022
VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
TABLESPACE USERS;

```
![](p1.png)

以上SQL语句创建了一个名为"orders"的表，其中包括订单号、客户姓名、客户电话、订单日期、雇员ID、折扣、应收账款等字段。表使用RANGE分区方式，根据订单日期分为4个分区：PARTITION_BEFORE_2016、PARTITION_BEFORE_2020、PARTITION_BEFORE_2021和PARTITION_BEFORE_2022。分别表示2016年以前、2016年到2019年、2020年到2021年和2021年到2022年。这个表的主键是订单号"order_id"，在表空间USERS中存储，不进行压缩和并行处理。此外，通过ALTER TABLE语句，可以在表中新增加一个名为PARTITION_BEFORE_2022的分区，用于存储订单日期早于2022年的订单数据。这样分区设计可以提高查询效率，并方便进行历史数据的归档和管理。

- 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
(
id NUMBER(9, 0) NOT NULL 
, order_id NUMBER(10, 0) NOT NULL
, product_id VARCHAR2(40 BYTE) NOT NULL 
, product_num NUMBER(8, 2) NOT NULL 
, product_price NUMBER(8, 2) NOT NULL 
, CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  )
, CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE USERS 
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
```
![](p2.png)


SQL语句创建了一个名为order_details的表格。这个表格包含了5个列：id、order_id、product_id、product_num和product_price。其中，id是一个9位数字，不能为空，是主键。order_id是一个10位数字，不能为空，是外键，参考了orders表的order_id列。product_id是一个最大长度为40字节的字符串，不能为空。product_num和product_price都是8位数字，不能为空。这个表格使用了USERS表空间，PCTFREE为10，INITRANS为1。这个表格没有启用压缩和并行处理。这个表格通过order_details_fk1外键参考了orders表的order_id列，启用了引用分区。
- 创建序列SEQ1的语句如下

```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
```
SQL 语句，用于创建名为 SEQ1 的序列（Sequence），该序列的起始值是 1，每次自增 1，最大值是 999999999，当序列达到最大值时将不再增加。同时，每次获取序列值时会缓存 20 个序列值，以减少访问序列对象的次数，提高序列获取效率。NOORDER 参数表示序列的生成顺序是无序的，NOCYCLE 表示当序列达到最大值时不会自动重新开始，NOKEEP 表示序列值不会被缓存到数据块中，NOSCALE 表示序列值不会缩放。GLOBAL 参数表示该序列是一个全局序列，可以被多个会话共享。

- 插入100条orders记录的脚本如下：

```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i := i+1;
    --在这里改变y,m,d
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) 
      values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/
```
![](p3.png)


这个PL/SQL代码会向 orders 表中插入 100 行数据，其中 order_id 是由 SEQ1 序列产生的连续数字，order_date 是从 2015 年 1 月 12 日开始，每个月加 1 个月直到 100 个订单，每个订单的日期都是以 "yyyy-MM-dd" 的格式存储在字符串 str 中，然后转换为日期格式插入到表中。在循环的每一次迭代中，月份 m 增加 1，如果月份超过了 12，则将其设置为 1，年份 y 和日期 d 保持不变。最后使用 commit 提交所有的数据更改。

- 插入100条order_details记录的脚本如下：
```sql
DECLARE
  i INTEGER;
  order_id NUMBER(10,0);
  product_id VARCHAR2(40 BYTE);
  product_num NUMBER(8,2);
  product_price NUMBER(8,2);
BEGIN
  i := 0;
  WHILE i < 100 LOOP
    i := i + 1;
    order_id := TRUNC(DBMS_RANDOM.VALUE(1, 100));
    product_id := 'product_' || TRUNC(DBMS_RANDOM.VALUE(1, 10));
    product_num := TRUNC(DBMS_RANDOM.VALUE(1, 10));
    product_price := TRUNC(DBMS_RANDOM.VALUE(10, 100));
    INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
      VALUES (SEQ1.NEXTVAL, order_id, product_id, product_num, product_price);
  END LOOP;
  COMMIT;
END;
/
```
![](p4.png)


这个脚本可以用来生成100条order_details记录，每条记录包含随机生成的order_id、product_id、product_num和product_price。其中order_id是从1到100中随机选择的，product_id是形如"product_1"的字符串，其中1是从1到10中随机选择的数字，product_num和product_price是从1到10和10到100中随机选择的整数。每次循环将一条新记录插入order_details表中，其中id是从SEQ1序列中获取的下一个值。最后通过COMMIT语句提交所有的修改。

- 联合查询的语句:
```sql
SELECT o.order_id, o.customer_name, o.order_date, SUM(d.product_num* d.product_price) AS total_price
FROM orders o
JOIN order_details d ON o.order_id = d.order_id
GROUP BY o.order_id, o.customer_name, o.order_date;
```
![](p5.png)


SQL查询语句使用了聚合函数SUM以及GROUP BY子句，用于查询所有订单的总价（总金额）。具体来说，该查询语句联结（JOIN）了orders表和order_details表，通过订单编号（order_id）关联这两个表。然后，对每个订单的编号（order_id）、客户名（customer_name）、订单日期（order_date）和订单中所有商品的总价（total_price）进行分组（GROUP BY）。在分组时，根据订单编号、客户名和订单日期来分组，并计算每个订单的总价。最终，该查询语句会返回一个结果集，包含所有订单的编号、客户名、订单日期和总价。

![](p6.png)


Oracle数据库生成的执行计划，它使用哈希连接来将两个表ORDERS和ORDER_DETAILS进行合并。该查询使用了一个全局索引来访问ORDERS表，使用了一个哈希分组来计算每个订单的总价格，并使用了分区引用来访问ORDER_DETAILS表。
具体的执行计划如下：
从ORDERS表访问100行数据，使用索引ORDERS_PK进行全索引扫描，这是一个唯一索引，用于加速查询。
将第一步的结果与ORDER_DETAILS表进行哈希连接，生成100行输出。
使用哈希分组计算每个订单的总价格。
从4个分区中的ORDER_DETAILS表访问500行数据，使用全表扫描操作。
将第四步的结果与哈希分组的结果进行合并，生成100行输出。
该执行计划使用的总成本是278，这是一个相对较低的成本，表明该查询可以在较短的时间内快速执行。

## 总结

本实验是一个简单的 SQL 数据库综合应用实验：
数据库表的创建与分区设计：通过创建orders和order_details两个表，建立主外键约束，以及对orders表按照年份进行分区设计，实现数据管理和查询优化。
数据的插入与删除：通过使用PL/SQL语言的循环和随机数函数，模拟了100条订单数据和100条订单明细数据的插入操作，并使用DELETE语句删除指定记录。
数据的查询与统计：通过JOIN操作连接orders和order_details两个表，实现了订单总金额的计算和分组统计。
通过本实验的练习，我们对SQL语言的应用和数据库管理的实际操作有了更深入的理解和掌握。同时，也可以借此加深对于分区设计和查询优化的了解，并提高了对于PL/SQL语言的应用能力。