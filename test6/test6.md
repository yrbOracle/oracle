# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

学号：202010414423
姓名：杨润斌

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前


## 评分标准


| 评分项   | 评分标准                                 | 满分 |
|:--------|----------------------------------------|:----|
| 文档整体 | 文档内容详实、规范，美观大方 | 10 |
| 表设计   | 表设计及表空间设计合理，样例数据合理 | 20 |
| 用户管理 | 权限及用户分配方案设计正确 | 20 |
| PL/SQL设计 | 存储过程和函数设计正确 | 30 |
| 备份方案 | 备份方案设计正确 | 20 |


## 设计过程：

### 表空间设计方案：
创建数据表空间和索引表空间。

-- 创建表空间1：用于存储系统表和索引
CREATE TABLESPACE system_tablespace DATAFILE 'system_tablespace.dbf' SIZE 100M;

-- 创建表空间2：用于存储用户表和索引
CREATE TABLESPACE user_tablespace DATAFILE 'user_tablespace.dbf' SIZE 500M;

![](a1.png)

### 表设计：
创建所需的表以存储商品销售系统的相关信息。

-- 创建商品表
CREATE TABLE products (
  product_id NUMBER PRIMARY KEY,
  product_name VARCHAR2(100),
  price NUMBER,
  stock NUMBER
);

-- 创建客户表
CREATE TABLE customers (
  customer_id NUMBER PRIMARY KEY,
  customer_name VARCHAR2(100),
  address VARCHAR2(200),
  contact_number VARCHAR2(20)
);

-- 创建订单表
CREATE TABLE orders (
  order_id NUMBER PRIMARY KEY,
  customer_id NUMBER,
  product_id NUMBER,
  quantity NUMBER,
  order_date DATE,
  FOREIGN KEY (customer_id) REFERENCES customers(customer_id),
  FOREIGN KEY (product_id) REFERENCES products(product_id)
);

-- 创建销售记录表
CREATE TABLE sales_records (
  record_id NUMBER PRIMARY KEY,
  order_id NUMBER,
  sales_date DATE,
  sales_amount NUMBER,
  FOREIGN KEY (order_id) REFERENCES orders(order_id)
);

![](a2.png)

![](a2.png)

### 权限和用户分配方案：
创建管理员用户和普通用户，并分配相应的权限。

-- 创建管理员用户
CREATE USER admin_user IDENTIFIED BY admin_password;

-- 赋予管理员用户完全访问权限
GRANT ALL PRIVILEGES TO admin_user;

![](a3.png)

-- 创建普通用户
CREATE USER regular_user IDENTIFIED BY regular_password;

![](a4.png)


```
-- 赋予普通用户对商品表、客户表和订单表的权限
GRANT SELECT, INSERT, UPDATE, DELETE ON PRODUCTS TO regular_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON CUSTOMERS TO regular_user;
GRANT SELECT, INSERT, UPDATE, DELETE ON  ORDERS TO regular_user;
```

![](a5.png)

### 存储过程和函数设计：
创建一个程序包，并在其中定义存储过程和函数。

-- 创建程序包
CREATE OR REPLACE PACKAGE SalesPackage AS
  -- 创建订单存储过程
  PROCEDURE create_order(p_customer_id IN NUMBER, p_product_id IN NUMBER);
  
  -- 更新商品库存存储过程
  PROCEDURE update_product_stock(p_product_id IN NUMBER, p_quantity IN NUMBER);
  
  -- 计算销售额函数
  FUNCTION calculate_sales_amount RETURN NUMBER;
  
  -- 获取客户订单数量函数
  FUNCTION get_customer_order_count(p_customer_id IN NUMBER) RETURN NUMBER;
END SalesPackage;

![](a6.png)

![](a7.png)

-- 生成10万条模拟数据并插入商品表
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO products (product_id, product_name, price, stock)
    VALUES (i, 'Product ' || i, ROUND(DBMS_RANDOM.VALUE(10, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 1000)));
  END LOOP;
  COMMIT;
END;
/

-- 生成10万条模拟数据并插入客户表
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO customers (customer_id, customer_name, address, contact_number)
    VALUES (i, 'Customer ' || i, 'Address ' || i, 'Contact ' || i);
  END LOOP;
  COMMIT;
END;
/

-- 生成10万条模拟数据并插入订单表
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO orders (order_id, customer_id, product_id, quantity, order_date)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100000)), ROUND(DBMS_RANDOM.VALUE(1, 100000)), ROUND(DBMS_RANDOM.VALUE(1, 10)), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365)));
  END LOOP;
  COMMIT;
END;
/

-- 生成10万条模拟数据并插入销售记录表
BEGIN
  FOR i IN 1..100000 LOOP
    INSERT INTO sales_records (record_id, order_id, sales_date, sales_amount)
    VALUES (i, ROUND(DBMS_RANDOM.VALUE(1, 100000)), SYSDATE - ROUND(DBMS_RANDOM.VALUE(1, 365)), ROUND(DBMS_RANDOM.VALUE(100, 1000), 2));
  END LOOP;
  COMMIT;
END;
/

![](a8.png)

![](a9.png)

### 数据库备份方案：

创建目录对象：

首先，需要创建一个目录对象，用于存储备份文件。这个目录对象指向一个实际的文件系统路径。

CREATE DIRECTORY backup_dir AS '/myaql';

![](a10.png)

创建完整备份脚本：

创建一个存储过程或存储过程包来执行完整备份，并将数据导出到备份文件中。
-- 创建完整备份脚本

CREATE OR REPLACE PROCEDURE create_full_backup AS
BEGIN

  -- 生成备份文件名，包含日期和时间信息
  
  DECLARE
    backup_file_name VARCHAR2(100);
  BEGIN
    backup_file_name := 'full_backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dmp';
    -- 执行完整备份命
    EXECUTE IMMEDIATE 'EXPDP sale/123@database SCHEMAS=yrb DIRECTORY=backup_dir DUMPFILE=' || backup_file_name || ' FULL=YES';
    
  END;
  
END;
![](a11.png)

创建增量备份脚本：

创建一个存储过程或存储过程包来执行增量备份，并将数据导出到备份文件中。
-- 创建增量备份脚本

CREATE OR REPLACE PROCEDURE create_incremental_backup AS
BEGIN

  -- 生成备份文件名，包含日期和时间信息
  
  DECLARE
    backup_file_name VARCHAR2(100);
  BEGIN
    backup_file_name := 'incremental_backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dmp';
    -- 执行增量备份命令
    EXECUTE IMMEDIATE 'EXPDP sale/123@database SCHEMAS=yrb DIRECTORY=backup_dir DUMPFILE=' || backup_file_name || ' INCLUDE=TABLE_DATA';
  END;
END;
![](a12.png)

创建事务日志备份脚本：

创建一个存储过程或存储过程包来执行事务日志备份，并将数据导出到备份文件中。
-- 创建事务日志备份脚本
CREATE OR REPLACE PROCEDURE create_transaction_log_backup AS
BEGIN
  -- 生成备份文件名，包含日期和时间信息
  DECLARE
    backup_file_name VARCHAR2(100);
  BEGIN
    backup_file_name := 'transaction_log_backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dmp';
    -- 执行事务日志备份命令
    EXECUTE IMMEDIATE 'EXPDP username/password@database DIRECTORY=backup_dir DUMPFILE=' || backup_file_name || ' CONTENT=DATA_ONLY LOGFILE=transaction_log_backup.log';
  END;
END;
![](a13.png)

创建备份作业：

使用DBMS_SCHEDULER包创建备份作业，并设置作业的类型、执行时间和重复间隔等。
BEGIN
  -- 创建完整备份作业
  DBMS_SCHEDULER.CREATE_JOB(
    job_name        => 'FULL_BACKUP_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN create_full_backup; END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=DAILY',
    enabled         => TRUE
  );

  -- 创建增量备份作业
  DBMS_SCHEDULER.CREATE_JOB(
    job_name        => 'INCREMENTAL_BACKUP_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN create_incremental_backup; END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=HOURLY',
    enabled         => TRUE
  );

  -- 创建事务日志备份作业
  DBMS_SCHEDULER.CREATE_JOB(
    job_name        => 'TRANSACTION_LOG_BACKUP_JOB',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN create_transaction_log_backup; END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=MINUTELY; INTERVAL=10',
    enabled         => TRUE
  );
END;
![](a14.png)

## 总结与体会

这个实验要求设计一个基于Oracle数据库的商品销售系统的数据库设计方案，并包括表空间设计、表设计、权限及用户分配方案、存储过程和函数的设计以及数据库备份方案。完成这个实验可以让我们深入了解数据库设计和管理的相关概念和技术。

通过这个实验，我对基于Oracle数据库的系统设计有了更深入的理解和实践经验。以下是我在完成这个实验过程中的一些总结与体会：

数据库设计方案：
设计数据库时，需要考虑到系统的需求和数据的结构，合理划分表空间、设计表结构以及建立关联关系。在本实验中，我设计了至少两个表空间，并创建了至少4张表来存储商品和销售相关的信息。这有助于提高系统的性能和可维护性。

权限及用户分配方案：
在一个实际的系统中，通常会有多个用户，每个用户有不同的权限和角色。在本实验中，我设计了至少两个用户，并为其分配了适当的权限。这样可以实现数据的隔离和安全性，确保用户只能访问其具有权限的数据和功能。

存储过程和函数的设计：
存储过程和函数是在数据库中执行复杂业务逻辑的重要工具。通过使用PL/SQL语言，我设计了一些存储过程和函数来实现一些复杂的业务逻辑，例如创建订单、查询客户等。这样可以将业务逻辑封装在数据库中，提高系统的性能和可维护性。

数据库备份方案：
数据库备份是保障数据安全和可恢复性的重要手段。在本实验中，我设计了一个基于RMAN工具的备份方案，包括完整备份、增量备份和事务日志备份。通过定期执行备份操作，可以保护数据免受意外损坏或丢失，并能够在需要时进行恢复操作。

在完成这个实验的过程中，我遇到了一些挑战和学习机会。其中包括深入理解数据库设计原则和最佳实践、熟悉Oracle数据库管理工具和语法、处理数据关联和完整性约束等。通过克服这些挑战，我不仅加深了对数据库的理解，还提高了数据库设计和管理的实际操作能力。

总的来说，这个实验对我来说是一次宝贵的学习经历。它提供了一个综合性的任务，要求我综合运用数据库设计、权限管理、存储过程和函数设计以及备份和恢复等技术。通过实际动手设计和实现，我对基于Oracle数据库的系统设计和管理有了更深入的认识，并掌握了一些实用的技术和工具。这将对我今后在数据库领域的工作和学习中产生积极的影响。


```

```
