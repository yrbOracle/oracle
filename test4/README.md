# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```
![](p7.png)

## 将杨辉三角源代码转为hr用户下的一个存储过程Procedure
```
CREATE OR REPLACE PROCEDURE YHTriangle (N IN INTEGER)
IS
  TYPE t_number IS VARRAY (100) OF INTEGER NOT NULL; --数组
  i INTEGER;
  j INTEGER;
  spaces VARCHAR2(30) :='   '; --三个空格，用于打印时分隔数字
  rowArray t_number := t_number();
BEGIN
  DBMS_OUTPUT.PUT_LINE('1'); --先打印第1行
  DBMS_OUTPUT.PUT(RPAD(1, N+1, ' ')); --先打印第2行第一个1
  DBMS_OUTPUT.PUT(RPAD(1, N+1, ' ')); --打印第二行第二个1
  DBMS_OUTPUT.PUT_LINE(''); --打印换行

  -- 初始化数组数据
  FOR i IN 1 .. N LOOP
    rowArray.EXTEND;
  END LOOP;
  rowArray(1) := 1;
  rowArray(2) := 1;

  FOR i IN 3 .. N -- 打印每行，从第3行起
  LOOP
    rowArray(i) := 1;
    j := i-1;

    -- 准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
    -- 这里从第j-1个数字循环到第2个数字，顺序是从右到左
    WHILE j > 1 LOOP
      rowArray(j) := rowArray(j) + rowArray(j-1);
      j := j - 1;
    END LOOP;

    -- 打印第i行
    FOR j IN 1 .. i LOOP
      DBMS_OUTPUT.PUT(RPAD(rowArray(j), N+1, ' ')); --打印数字
    END LOOP;

    DBMS_OUTPUT.PUT_LINE(''); --打印换行
  END LOOP;
END;
/
```
![](p10.png)

存储过程 YHTriangle(N) 接受一个整数参数 N，表示要打印的杨辉三角的行数。它使用了一个 VARRAY 类型的数组 rowArray 来存储每行的数字，其中第一个元素和第二个元素都初始化为 1。然后，它使用两个循环来打印每一行的数字。外层循环从第 3 行开始，一直循环到第 N 行，内层循环用来打印每行的数字。在打印每个数字之前，使用 RPAD 函数将数字格式化为占据 N+1 个字符的字符串，以便打印出来的数字对齐。最后，使用 DBMS_OUTPUT.PUT_LINE 和 DBMS_OUTPUT.PUT 函数来打印数字和换行符，以便在 SQL Plus 窗口中显示出整个杨辉三角。


## 创建YHTriange
```
EXEC YHTriangle(12);

```
![](p9.png)

## 实验结论
根据我们的存储过程实验结果，当输入参数为正整数N时，程序能够正确地输出N行杨辉三角。这表明该存储过程能够成功地生成杨辉三角，同时也证明了PL/SQL语言的基本功能和数组的使用。我们可以通过该存储过程在Oracle数据库中生成任意行数的杨辉三角。
通过将源代码转为存储过程 YHTriangle，我们可以更方便地生成杨辉三角，并且可以根据需要指定要打印的行数。存储过程可以在数据库中长期保存，重复使用，而不需要每次都重新输入源代码，提高了效率。此外，存储过程还可以作为其他程序或存储过程的组成部分，进一步提高了代码重用性和可维护性。
