# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

## 脚本代码参考


```python
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
```

![](s1.png)

这段代码创建了一个名为MyPack的PL/SQL包，包头定义了三个PL/SQL程序单元：一个函数和两个过程。
函数Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)返回一个部门的薪资总额，它接收一个数字类型的部门ID作为输入参数，返回一个数字类型的薪资总额。
过程Get_Employees(V_EMPLOYEE_ID NUMBER)递归查询某个员工及其所有下属，子下属员工，它接收一个数字类型的员工ID作为输入参数，不返回任何结果，而是直接输出查询结果。


```python
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
```

![](s2.png)

这是一个包(MyPack)的主体(package body)，里面定义了两个子程序，包括一个函数(Get_SalaryAmount)和一个存储过程(GET_EMPLOYEES)。函数Get_SalaryAmount计算给定部门ID的工资总额，存储过程GET_EMPLOYEES用递归查询从给定员工ID开始的所有下属员工。

在Get_SalaryAmount中，使用了一个SELECT查询，将给定部门ID的所有员工的工资总和作为一个NUMBER类型的变量N返回。在Get_Employees中，使用了一个游标和一个连接查询(SELECT ... CONNECT BY...)，以递归方式获取所有下属员工的信息。

## 测试


```python
# 函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

![](s3.png)

![](s4.png)

SQL查询语句可以在数据库中查询部门表(departments)，同时使用MyPack包中的Get_SalaryAmount函数计算每个部门的薪资总额，最终结果将列出每个部门的ID，名称和薪资总额。


```python
# 过程Get_Employees()测试代码：
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
```

![](s5.png)

是一个匿名PL/SQL块，设置了一个变量V_EMPLOYEE_ID的值为101，然后调用了MyPack包中的Get_Employees过程，将V_EMPLOYEE_ID作为参数传入过程中进行查询操作，并将结果通过DBMS_OUTPUT.PUT_LINE输出到输出窗口中。最后通过END语句结束这个PL/SQL块。

## 总结

在本次实验中，我们学习了Oracle数据库中包(package)和包体(package body)的创建和使用。

包可以看做是一组数据库对象的集合，这些对象包括变量、常量、游标、函数、过程等等。包体是包中各个对象的实现代码。

在本次实验中，我们创建了一个名为MyPack的包，其中包含一个函数Get_SalaryAmount和一个过程Get_Employees。Get_SalaryAmount函数可以根据输入的部门ID，统计该部门的工资总额；Get_Employees过程可以通过递归查询某个员工及其所有下属，子下属员工。

我们还学习了如何在SQL Plus中使用SELECT语句调用包中的函数，并且可以根据需要为函数起一个别名；同时我们还学习了如何在PL/SQL中调用包中的过程。在PL/SQL中，我们需要先声明一个变量，然后使用包名和过程名来调用过程，同时可以将需要传递的参数传递给过程。

总之，Oracle数据库中的包可以大大简化数据库应用程序的开发和维护，通过将相关的功能和代码组织在一个包中，可以实现更好的封装和复用。同时，包还可以提高数据库应用程序的安全性和性能。


```python

```
